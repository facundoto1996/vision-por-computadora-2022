import numpy as np
import cv2

patron=0.04926108374
a=0
l=0
drawing=False
ix=0
iy=0
ix2=0
iy2=0

def perspective(image, src_pts, dst_pts):
    (h, w) = image.shape[:2]
    M = cv2.getPerspectiveTransform(src_pts, dst_pts)
    rectified = cv2.warpPerspective(img, M, (w, h), borderMode=cv2.BORDER_CONSTANT)
    return rectified

def puntos(event, x, y, flags, param):
    global selected_points
    if event == cv2.EVENT_LBUTTONDOWN:
        ix,iy = x,y
        selected_points.append([x, y])

        pointx=str("ix:{} iy:{}".format(ix, iy))
        print("Pointex: {}".format(pointx))
        cv2.putText(img, text=pointx, org=(ix, iy), fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=0.25, color=(0, 0, 0),thickness=1)
def draw_rectangle(event, x, y, flags, params):
    global ix, iy, ix2, iy2, drawing, img_backup, img, a,l
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix, iy = x, y
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        ix2, iy2 = x, y

        cv2.rectangle(img, (ix, iy), (x, y), (0, 255, 0), 1)
        if ix<x:
            dif_x = x-ix
        else:
            dif_x = ix-x
  
        if iy<y:
            dif_y = y-iy
        else:
            dif_y = iy-y

        
        print("Alto:"+ str(int(a*dif_y)) +", Largo:"+ str(int(l*dif_x)))

        area="Alto:"+ str(int(a*dif_y)) + ", Largo:"+ str(int(l*dif_x))
        cv2.putText(img, text=area, org=(ix, iy), fontFace=cv2.FONT_HERSHEY_TRIPLEX, fontScale=0.8, color=(0, 0, 0),thickness=1)




selected_points=[]
destin_points=[]

img = cv2.imread('Prueba2_Hor.jpeg')
img_backup = 'Prueba2_Hor.jpeg'
cv2.namedWindow('Oprima_el_boton_derecho_del_mouse_arrastre_y_suelte_para_obtener_medida')
cv2.setMouseCallback('Oprima_el_boton_derecho_del_mouse_arrastre_y_suelte_para_obtener_medida', puntos)


Bup= img.copy()

while True:
    cv2.imshow('Oprima_el_boton_derecho_del_mouse_arrastre_y_suelte_para_obtener_medida', img)
    option = cv2.waitKey(1) & 0b11111111 
    if option == ord('h'):   
        img = Bup.copy()
        m = Bup.copy()

        # while True:     #!Seleciona los 4 puntos
        #     k = cv2.waitKey(1)
        #     if len(selected_points) == 4:
        #         break

        # x=[]
        # y=[]    

        # for par in selected_points:
        #     x.append(par[0])
        #     y.append(par[1])
    
        x=[431,648,635,392]
        y=[203,209,527,529]
        x1,x2,x3,x4 = min(x),max(x),max(x),min(x)
        y1,y2,y3,y4 = min(y),min(y),max(y),max(y)

        dst_pts= np.array([[x1,y1],[x2,y2],[x3,y3],[x4,y4]],dtype=np.float32)
        src_pts = np.array([[x[0],y[0]],[x[1],y[1]],[x[2],y[2]],[x[3],y[3]]],dtype=np.float32)

        img = perspective(img, src_pts, dst_pts)
        
        a=float(86/(y3-y1))
        l=float(54/(x2-x1))

        cv2.destroyAllWindows()
        cv2.imshow('Oprima_el_boton_derecho_del_mouse_arrastre_y_suelte_para_obtener_medida', img)
        cv2.namedWindow('Oprima_el_boton_derecho_del_mouse_arrastre_y_suelte_para_obtener_medida')
        cv2.setMouseCallback('Oprima_el_boton_derecho_del_mouse_arrastre_y_suelte_para_obtener_medida', draw_rectangle)

        
        
    elif option == ord('g'):
        cv2.imwrite('rectificado.png', img)  
    
    elif option == ord('q'):
        break