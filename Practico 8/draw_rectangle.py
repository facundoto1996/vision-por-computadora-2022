import cv2
from transf_euclideana import transf_euclideana1
from transf_euclideana import similarity
import numpy as np
"""
Global VAR Definition
"""
image = {
    "ix":0,
    "iy":0,
    "xf":0,
    "yf":0,
    "angle":0, 
    "tx":0,
    "ty":0,
    "scale": 4.0,
}
save_cap= []
cap_num = 0



imgp = cv2.imread('im.jpg', cv2.IMREAD_COLOR)
imgp = cv2.resize(imgp,(800, 600))
xi, yi = 0, 0
xf, yf = imgp.shape[1], imgp.shape[0]
backup = imgp.copy()
roi = imgp.copy()

def puntos(event, x, y, flags, param):
    global selected_points, image
    if event == cv2.EVENT_LBUTTONDOWN:
        selected_points.append([x, y])
"""
draw_rectangle() 
Dibuja un rectangulo en la imagen
"""
def draw_rectangle(event,x,y,flags,param):
  global ix,iy,save_cap
  w = 2
  
  if event == cv2.EVENT_LBUTTONDOWN:
    ix,iy = x,y

    cv2.rectangle(imgp,(ix, iy ),(x,y),(255,0,0),1)

  elif event == cv2.EVENT_LBUTTONUP:
    cv2.rectangle(imgp,(ix,iy),(x,y),(255,0,0),w)
    if ix>x:
      dif_x = [x,ix]
    else:
      dif_x = [ix,x]
  
    if ix>x:
      dif_y = [y,iy]
    else:
      dif_y = [iy,y]
      
    save_cap = [dif_y, dif_x]

def perspective(image, src_pts, dst_pts):
    (h, w) = image.shape[:2]
    M = cv2.getPerspectiveTransform(src_pts, dst_pts)
    rectified = cv2.warpPerspective(imgp, M, (w, h), borderMode=cv2.BORDER_CONSTANT)
    return rectified

def save_snip():
  global imgp
  img_name= 'capture.png'
  if save_cap:
    capture = imgp[save_cap[0][0]+1:save_cap[0][1]-1, save_cap[1][0]+1:save_cap[1][1]-1]
    cv2.imwrite(img_name, capture)
    save_cap.clear()
    return True,capture

  return False,0
cv2.namedWindow('Practico 6')
cv2.setMouseCallback('Practico 6', draw_rectangle)  #! Esto tengo que cambiarlo


while(True):
    cv2.imshow('Practico 6', imgp)
    option = cv2.waitKey(1) & 0xff    #Enmascaro con una AND
    if option == ord('r'):
        p = backup.copy()
        roi = backup.copy()
        xi, yi = 0, 0
        xf, yf = imgp.shape[1], imgp.shape[0]
    if save_cap:
      yi, yf = save_cap[0][0], save_cap[0][1]
      xi, xf = save_cap[1][0], save_cap[1][1]

      xi, xf = min(xi, xf), max(xi, xf)
      yi, yf = min(yi, yf), max(yi, yf)
      roi = backup[yi:yf, xi:xf]

    if option == ord('g'):
        ret = save_snip()[0]
        if ret:
            imgp = cv2.imread('im.jpg',cv2.COLOR_BGR2RGB)
            cv2.imshow('Recorte',ret)
        else:
            print('Selecione un recorte a guardar')
    
    elif option == ord('r'):
      ret,snip = save_snip()
      if ret:
        print("\nIngrese los parámetros para aplicar transf. Euclideana:")
        angle = int(input("\nAngulo: "))
        tx = int(input("\nDespl. x: "))
        ty = int(input("\nDespl. y: "))

        ret = transf_euclideana1(snip,angle,tx, ty)
        if ret: 
          background = cv2.imread('imagen_transformada.png',cv2.COLOR_BGR2RGB)
          cv2.imshow('imagen_transformada.png',background)
          imgp = cv2.imread('im.jpg',cv2.COLOR_BGR2RGB)
      else:
        print('Selecione un recorte para transformar')

    elif option == ord('s'):
        print("\nIngrese los parámetros para aplicar transf. de Similaridad:")
        image["angle"] = int(input("\nAngulo: "))
        image["tx"] = int(input("\nDespl. x: "))
        image["ty"] = int(input("\nDespl. y: "))
        image["scale"] = int(input("\nEscala. y: "))
        roi = similarity(roi, image["angle"], image["tx"], image["ty"], image["scale"])
        imgp = roi.copy()
        cv2.imwrite('python_similarity.png', roi)
    elif option == ord('a'):
        cv2.destroyAllWindows()
        cv2.imshow('Practico 6', imgp)

        selected_points=[]
        selected_points.clear()
        cv2.namedWindow('Seleccione 3 Puntos')
        cv2.setMouseCallback('Seleccione 3 Puntos', puntos)

        while True:
            cv2.imshow('Seleccione_3_Puntos', imgp)
            k = cv2.waitKey(1)
            if len(selected_points) == 3:
                break
        cv2.destroyAllWindows()
        srcTri = np.array( [[0, 0], [imgp.shape[1] - 1, 0], [0, imgp.shape[0] - 1]] ).astype(np.float32)
        dstTri = np.array( [selected_points[0], selected_points[1], selected_points[2]] ).astype(np.float32)
        a = cv2.getAffineTransform(srcTri, dstTri)
        print("ix  es {} es {} es {}".format(selected_points[0], selected_points[1], selected_points[2]))
        outputim = np.zeros(imgp.shape)
        affine= cv2.warpAffine(imgp, a, (imgp.shape[1], imgp.shape[0]))
        #cv2.imwrite('Affine', affine)
        cv2.imshow('Resultado7', affine)
    elif option == ord('h'):
        cv2.destroyAllWindows()
        cv2.imshow('Practico 6', imgp)

        selected_points=[]
        selected_points.clear()
        cv2.namedWindow('Practico 6')
        cv2.setMouseCallback('Practico 6', puntos)
        while True:     #!Seleciona los 4 puntos
          k = cv2.waitKey(1)
          if len(selected_points) == 4:
            break

        x=[]
        y=[]    

        for par in selected_points:
            x.append(par[0])
            y.append(par[1])
    

        x1,x2,x3,x4 = min(x),max(x),max(x),min(x)
        y1,y2,y3,y4 = min(y),min(y),max(y),max(y)
        cv2.destroyAllWindows()
        dst_pts= np.array([[x1,y1],[x2,y2],[x3,y3],[x4,y4]],dtype=np.float32)
        src_pts = np.array([[x[0],y[0]],[x[1],y[1]],[x[2],y[2]],[x[3],y[3]]],dtype=np.float32)

        imgp = perspective(imgp, src_pts, dst_pts)
        cv2.destroyAllWindows()
        cv2.imshow('Practico 6', imgp)
        # while True:
        #     cv2.imshow('Seleccione_4_Puntos', imgp)
        #     k = cv2.waitKey(1)
        #     if len(selected_points) == 4:
        #         break
        # cv2.destroyAllWindows()
        # srcTri= np.array( [[0, 0], [0, 600], [800, 0],[800,600]]).astype(np.float32)
        # dstTri = np.array( [selected_points[0], selected_points[1], selected_points[2], selected_points[3]] ).astype(np.float32)
        # a = cv2.getPerspectiveTransform(srcTri, dstTri)
        # print("ix  es {} es {} es {} es {}".format(selected_points[0], selected_points[1], selected_points[2], selected_points[3]))
        # outputim = np.zeros(imgp.shape)
        # perspective= cv2.warpPerspective(imgp, a, (imgp.shape[1], imgp.shape[0]))
        # #cv2.imwrite('Affine', affine)
        # cv2.imshow('Resultado7', perspective)
    elif option == ord('q'):
        break