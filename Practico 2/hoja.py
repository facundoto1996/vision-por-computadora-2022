#! /usr/bin/env python
# -*- coding: utf-8 -*-
# from os import WUNTRACED
import cv2
import numpy as np
import os
img = cv2.imread('hoja.png',0)

height, width,= img.shape[:2]

colorScale = int(input('Ingrese gama de gris a filtrar\n'))

# Agregar código aquí
for h in range (height):
  for w in range (width):
    # print(img[h][w])
    if(img[h][w]<=colorScale):
      img[h][w]=0

# Para resolverlo podemos usar dos for anidados
cv2.imwrite('resultado.png',img)
cv2.imshow("resultado.png", img)
k = cv2.waitKey(0)
if k == 27:         # wait for ESC key to exit
    cv2.destroyAllWindows()