import cv2
from transf_euclideana import transf_euclideana1
from transf_euclideana import similarity

"""
Global VAR Definition
"""
image = {
    "ix":0,
    "iy":0,
    "xf":0,
    "yf":0,
    "angle":0, 
    "tx":0,
    "ty":0,
    "scale": 4.0,
}
save_cap= []
cap_num = 0



imgp = cv2.imread('im.jpg', cv2.IMREAD_COLOR)
imgp = cv2.resize(imgp,(800, 600))
xi, yi = 0, 0
xf, yf = imgp.shape[1], imgp.shape[0]
backup = imgp.copy()
roi = imgp.copy()


"""
draw_rectangle() 
Dibuja un rectangulo en la imagen
"""
def draw_rectangle(event,x,y,flags,param):
  global ix,iy,save_cap
  w = 2
  
  if event == cv2.EVENT_LBUTTONDOWN:
    ix,iy = x,y

    cv2.rectangle(imgp,(ix, iy ),(x,y),(255,0,0),1)

  elif event == cv2.EVENT_LBUTTONUP:
    cv2.rectangle(imgp,(ix,iy),(x,y),(255,0,0),w)
    if ix>x:
      dif_x = [x,ix]
    else:
      dif_x = [ix,x]
  
    if ix>x:
      dif_y = [y,iy]
    else:
      dif_y = [iy,y]
      
    save_cap = [dif_y, dif_x]
def save_snip():
  global imgp
  img_name= 'capture.png'
  if save_cap:
    capture = imgp[save_cap[0][0]+1:save_cap[0][1]-1, save_cap[1][0]+1:save_cap[1][1]-1]
    cv2.imwrite(img_name, capture)
    save_cap.clear()
    return True,capture

  return False,0
cv2.namedWindow('Practico 6')
cv2.setMouseCallback('Practico 6', draw_rectangle)  #! Esto tengo que cambiarlo


while(True):
    cv2.imshow('Practico 6', imgp)
    option = cv2.waitKey(1) & 0xff    #Enmascaro con una AND
    if option == ord('r'):
        p = backup.copy()
        roi = backup.copy()
        xi, yi = 0, 0
        xf, yf = imgp.shape[1], imgp.shape[0]
    if save_cap:
      yi, yf = save_cap[0][0], save_cap[0][1]
      xi, xf = save_cap[1][0], save_cap[1][1]

      xi, xf = min(xi, xf), max(xi, xf)
      yi, yf = min(yi, yf), max(yi, yf)
      roi = backup[yi:yf, xi:xf]

    if option == ord('g'):
        imgp = roi.copy()
        cv2.imwrite('python_crop.png', roi)
    
    elif option == ord('r'):
      ret,snip = save_snip()
      if ret:
        print("\nIngrese los parámetros para aplicar transf. Euclideana:")
        angle = int(input("\nAngulo: "))
        tx = int(input("\nDespl. x: "))
        ty = int(input("\nDespl. y: "))

        ret = transf_euclideana1(snip,angle,tx, ty)
        if ret: 
          background = cv2.imread('imagen_transformada.png',cv2.COLOR_BGR2RGB)
          cv2.imshow('imagen_transformada.png',background)
          imgp = cv2.imread('im.jpg',cv2.COLOR_BGR2RGB)
      else:
        print('Selecione un recorte para transformar')

    elif option == ord('s'):
        print("\nIngrese los parámetros para aplicar transf. de Similaridad:")
        image["angle"] = int(input("\nAngulo: "))
        image["tx"] = int(input("\nDespl. x: "))
        image["ty"] = int(input("\nDespl. y: "))
        image["scale"] = int(input("\nEscala. y: "))
        roi = similarity(roi, image["angle"], image["tx"], image["ty"], image["scale"])
        imgp = roi.copy()
        cv2.imwrite('python_similarity.png', roi)
    
    elif option == ord('q'):
        break