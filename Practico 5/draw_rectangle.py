#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
from transf_euclideana import *

ix,iy = 0,0
save_cap= []
cap_num = 0

imgp = cv2.imread('im.jpg',cv2.COLOR_BGR2RGB)

def draw_rectangle(event,x,y,flags,param):
  global ix,iy,save_cap
  if event == cv2.EVENT_LBUTTONDOWN:
    ix,iy = x,y

    cv2.rectangle(imgp,(ix, iy ),(x,y),(255,0,0),1)

  elif event == cv2.EVENT_LBUTTONUP:
    cv2.rectangle(imgp,(ix,iy),(x,y),(255,0,0),1)
    if ix>x:
      dif_x = [x,ix]
    else:
      dif_x = [ix,x]
  
    if ix>x:
      dif_y = [y,iy]
    else:
      dif_y = [iy,y]

    save_cap = [dif_y, dif_x]


def save_snip():
  global imgp
  img_name= 'capture.png'
  if save_cap:
    capture = imgp[save_cap[0][0]+1:save_cap[0][1]-1, save_cap[1][0]+1:save_cap[1][1]-1]
    cv2.imwrite(img_name, capture)
    save_cap.clear()
    return True,capture

  return False,0

cv2.namedWindow('Bosquecito')
cv2.setMouseCallback('Bosquecito',draw_rectangle)

while(1):
  cv2.imshow('Bosquecito',imgp)
  k=cv2.waitKey(1)&0xFF

  if k == ord('g'):
    ret = save_snip()[0]
    if ret:
      imgp = cv2.imread('im.jpg',cv2.COLOR_BGR2RGB)
    else:
      print('Selecione un recorte a guardar')

  elif k == ord('r'): #?Aplicar transformacion Euclideana a la porcion de imagen que ya recortamos.
    ret,snip = save_snip()
    if ret:
      print("\nIngrese los parámetros para aplicar transf. Euclideana:")
      angle = int(input("\nAngulo: "))
      tx = int(input("\nDespl. x: "))
      ty = int(input("\nDespl. y: "))

      ret = transf_euclideana(snip,angle,tx, ty)
      if ret: 
        background = cv2.imread('imagen_transformada.png',cv2.COLOR_BGR2RGB)
        cv2.imshow('imagen_transformada.png',background)
        imgp = cv2.imread('im.jpg',cv2.COLOR_BGR2RGB)
    else:
      print('Selecione un recorte para transformar')

        
  elif k == 27 or cv2.waitKey(1)& 0xFF == ord('q'):
    break

cv2.destroyAllWindows()

