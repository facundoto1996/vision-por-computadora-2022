# #! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np
from math import cos, sin


def transf_euclideana(image,angle=0,tx=0,ty=0):

  angle = angle*2* np.pi /360

  (h,w)= (image.shape[0],image.shape[1])

  #? Matriz necesaria para transformacion Euclideana
  M = np.matrix([
  [cos(angle),sin(angle),tx],
  [-sin(angle),cos(angle),ty],
  [0,0,1]])

  #? Matriz utilizada para girar sobre el centro de la imagen
  T1 = np.matrix ([[1, 0, w/2],
                  [0, 1, h/2],
                  [0, 0, 1]])

  T2 = np.matrix ([[1, 0, -w/2],
                    [0, 1, -h/2],
                    [0, 0, 1]])

  #!Matriz de transf T1*A*T2
  #? np.dot -> Producto de dos arrays.
  Matrix = T1.dot(M.dot(T2))

  background = np.zeros_like(image)
  # cv2.imshow('background',background)

  for i in range(h): #recorre la matriz y los colores
    for j in range(w):
        for k in range(3):
            orig = np.array([[i],[j],[1]])  # guarda las coordenads
                                            #originales del pixel
            dest_x,dest_y,one = Matrix.dot(orig)    #aplica la transf
            dest_x = int(dest_x)            
            dest_y = int(dest_y)
            
            if ((dest_y < w) and (dest_x < h) and (dest_y > 0)and(dest_x > 0)):
                            background[i,j,k]=image[dest_x,dest_y,k]

  ret = cv2.imwrite('imagen_transformada.png',background )
  return ret
