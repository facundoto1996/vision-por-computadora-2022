#! /usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import cv2

if(len(sys.argv)> 1):
  filename = sys.argv[1]
  cap = cv2.VideoCapture(filename)
else:
  print('Opening first webcam by default')
  cap = cv2.VideoCapture(0)

if not cap.isOpened():
    print("Cannot open camera or source video\n")
    sys.exit(0) 


fourcc = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')

fps = cap.get(cv2.CAP_PROP_FPS)  
framesize=(int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))

print(fourcc, fps, framesize)

delay = int(1000/fps) 
# ret, frame = cap.read() 
out = cv2.VideoWriter('output_XVID.avi',fourcc,fps,framesize, isColor = False)

while(cap.isOpened()):
  ret, frame = cap.read()
  if ret is True:
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    out.write(gray)
    cv2.imshow('Image gray',gray)
    if cv2.waitKey(delay)& 0xFF == ord('q'):
        break
  else:
    break

cap.release()
out.release()
cv2.destroyAllWindows() 
