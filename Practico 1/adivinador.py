import random
from time import sleep

numero = random.randint(0, 100)

def adivina(intentos):
  while intentos > 0:
    guess= int(input('Ingresa un valor valido para ganar! 👾 '))
    if guess == numero:
      print('Adivinaste! El valor correcto era {} 🎉✨!'.format(numero))
      sleep(2)
      exit()
    else:
      intentos=intentos-1
      if intentos == 0:
        print('Perdiste! 🙊. Ya no hay mas intentos disponibles...')
        sleep(2)
      else:
        print('No adivinaste. Intenta nuevamente... 💪 . Intentos disponibles: ', intentos)


intentos = int(input('Ingresa el numero de intentos para adivinar 🎈\n'))
adivina(intentos)